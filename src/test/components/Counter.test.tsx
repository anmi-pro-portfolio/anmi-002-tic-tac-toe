import { render } from "@testing-library/react";

import { Counter } from '../../components/Counter';


describe('Counter Component Test', () => {

  test('should render component', () => {

    const handleChange = jest.fn(console.log);

    const mount = render( <Counter counter={0} handleOnChange={handleChange} /> );

    mount.findAllByText('0').then((e) => {
      console.log(e);
    })

    // console.log(handleChange.mock.calls);

  })

})



