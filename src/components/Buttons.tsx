
interface buttonsProps {
  increment(): void 
  decrement(): void 
  reset(): void 
}

export const Buttons = ({ decrement, increment, reset }: buttonsProps) => (
  <div className="buttons__action">
    <button
      className="button action--decrement"
      onClick={decrement}
    > - </button>
    <button
      className="button action--reset"
      onClick={reset}
    > 0 </button>
    <button
      className="button action--increment"
      onClick={increment}
    > + </button>
  </div>
);