import React from 'react';

interface CounterProps {
  counter: number
  handleOnChange( e: React.FormEvent<HTMLInputElement> ): void
}

export const Counter = ({ counter, handleOnChange } : CounterProps ) => (
  <div className="input__counter">
    <input type="number" value={ counter } onChange={handleOnChange} />
  </div>
);
