import React, { useState } from "react";
import { Buttons } from "../components/Buttons";
import { Counter } from "../components/Counter";

export const CounterApp = () => {
  const initCounter: number = 0;
  const [counter, setCounter] = useState(initCounter);

  const increment = () => setCounter((prevCount) => prevCount + 1);
  const decrement = () => setCounter((prevCount) => prevCount - 1);
  const reset = () => setCounter(initCounter);

  const handleOnChange = ({
    currentTarget,
  }: React.FormEvent<HTMLInputElement>) => {
    setCounter(parseInt(currentTarget.value));
  };

  return (
    <section className="container__main">
      <Counter counter={counter} handleOnChange={handleOnChange} />
      <Buttons increment={increment} decrement={decrement} reset={reset} />
    </section>
  );
};
